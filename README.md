# Project Name: ButtHurt Form
### Description: Allowing of IRC users to submit a form online (known as ButtHurt Form).
#### Author: Taek
#### Author URI: https://taek.us/
#### Project URI: http://git.taek.us/taek/butthurtform
#### License: GPLv2
#### This is a backup repository in case anything happens to the main repo.